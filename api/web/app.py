from flask import Flask, Blueprint
from .restplus import api
from ..models import db
from .config import config

from ..web.endpoints.user import ns as ns_user

app = Flask(__name__)
app.config.from_object(config)

# Init DB
db.init_app(app)
app.app_context().push()

# Blueprint
blueprint = Blueprint('api', __name__)
api.init_app(blueprint)
api.add_namespace(ns_user)
app.register_blueprint(blueprint)
