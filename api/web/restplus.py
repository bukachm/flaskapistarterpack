from flask_restplus import Api
from .config import config

api = Api(version='1.0', title=config.PROJECT_NAME, description=config.PROJECT_DESCRIPTION)
