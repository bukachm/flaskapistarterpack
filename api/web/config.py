import os


class Config(object):
    PROJECT_NAME = 'Api Starter pack'
    PROJECT_DESCRIPTION = 'Python Flask Api Starter pack for Topcoder challenge'

    # Flask settings
    HOST = '0.0.0.0'
    PORT = 5000
    THREADED = True
    DEBUG = False
    TESTING = False

    # Flask-Sqlalchemy
    SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI", 'sqlite:///db.sqlite')
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    # Flask-Restplus settings
    RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
    RESTPLUS_VALIDATE = True
    RESTPLUS_MASK_SWAGGER = False
    RESTPLUS_ERROR_404_HELP = False

    # Flask-Security config
    SECURITY_PASSWORD_HASH = "pbkdf2_sha512"
    SECURITY_PASSWORD_SALT = "ATGUOHAELKiubahiughaerGOJAEGj"
    SECRET_KEY = 'Api-Starter-pack'
    SECURITY_KEY = 'basicAuth'


class ProductionConfig(Config):
    DEBUG = False


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
    DEBUG = True


config = TestingConfig()
print(config.SQLALCHEMY_DATABASE_URI)
