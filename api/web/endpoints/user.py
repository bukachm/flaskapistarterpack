from flask import request
from flask_restplus import Resource
from ..restplus import api
from api.web.serializers import error_response, success_response
from ..serializers.users import s_users, s_user_post, s_users_paginated
from ..parsers.pagination import pagination_arguments
from ...models import User

ns = api.namespace('users', description='Operations related to user')


@ns.route('/')
class UsersRoute(Resource):
    @api.expect(pagination_arguments)
    @api.marshal_with(s_users_paginated)
    def get(self):
        """

        Get all users.

        """
        args = pagination_arguments.parse_args(request)
        page = args.get('page', 1)
        per_page = args.get('per_page', 10)

        query = User.query
        users = query.paginate(page, per_page, error_out=False)

        return users, 200

    @api.expect(s_user_post)
    @api.marshal_with(s_users)
    def post(self):
        """

        Create new user.

        """
        data = request.json

        user = User(handle=data.get('handle'))

        if user.save():
            return success_response(message='User created successfully', data=[user]), 201

        return error_response(message='Error'), 500


@ns.route('/<int:user_id>')
class UserRoute(Resource):
    @api.expect(s_user_post)
    @api.marshal_with(s_users)
    def put(self, user_id):
        """

        Update user by id.

        """
        data = request.json
        user = User.query.get(user_id)

        if user:
            user.handle = data.get('handle')
            user.save()

            return success_response(data=[user]), 201

        return error_response(message='User not found.'), 404

    @api.marshal_with(s_users)
    def delete(self, user_id):
        """

        Delete user by id.

        """

        user = User.query.get(user_id)
        if user:
            user.delete()
            return success_response(message='User successfully deleted.'), 201

        return error_response(message='User not found.'), 404
