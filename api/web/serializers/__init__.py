from flask_restplus import fields
from ...web.restplus import api


class Message:
    def __init__(self, message=None, type="error"):
        self.message = message
        self.type = type


class Response:
    def __init__(self, success=True, messages=[], data=[]):
        self.success = success
        self.messages = messages
        self.data = data


def error_response(message=None, message_type='error', data=None, success=False):
    if message:
        message = Message(type=message_type, message=message)
        return Response(success=success, data=data, messages=[message])

    return Response(success=success, data=data)


def success_response(message=None, message_type='ok', data=None, success=True):
    if message:
        message = Message(type=message_type, message=message)
        return Response(success=success, data=data, messages=[message])

    return Response(success=success, data=data)


s_message = api.model('Message', {'message': fields.String(description='Message'),
                                  'type': fields.String(description='Message type')
                                  })

s_response = api.model('Response', {
    'success': fields.Boolean,
    'messages': fields.List(fields.Nested(s_message))
})
