from flask_restplus import fields
from ...web.restplus import api
from ...web.serializers import s_response
from .pagination import s_pagination

s_user = api.model('User', {
    'id': fields.Integer(description='The unique identifier'),
    'handle': fields.String(description='Handle')
})

s_user_post = api.model('UserPost', {
    'handle': fields.String(required=True, description='Handle')
})

s_users = api.inherit('Users', s_response,
                      {'data': fields.List(fields.Nested(s_user, skip_none=True), skip_none=True)
                       })

s_users_paginated = api.inherit('UsersPaginated', s_pagination, {'items': fields.List(fields.Nested(s_user))})