from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class CRUD():
    def save(self):

        try:
            if hasattr(self, 'id'):
                if self.id == None:
                    db.session.add(self)
            else:
                db.session.add(self)

            db.session.commit()
            return True
        except Exception as e:
            print(e)
            return False

    def delete(self):
        db.session.delete(self)
        return db.session.commit()


from .user import User
