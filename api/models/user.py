from sqlalchemy import Column, Integer, String
from ..models import db, CRUD
from ..web.config import config

SECRET_KEY = config.SECRET_KEY


class User(db.Model, CRUD):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    handle = Column(String(255), nullable=False)

    def __init__(self, handle=None):
        self.handle = handle

    def __repr__(self):
        return 'User: {0}'.format(self.handle)
