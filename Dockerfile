FROM python:3.7-alpine

WORKDIR /home/flask-api-sp

COPY requirements.txt .

RUN \
 apk add --no-cache python3 postgresql-libs && \
 apk add --no-cache --virtual .build-deps gcc python3-dev musl-dev postgresql-dev && \
 apk add --no-cache libressl-dev libffi-dev libxslt-dev jpeg-dev zlib-dev && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps && \
 rm -rf /var/cache/apk/*

COPY ./api api/
COPY ./manage.py .
COPY ./run.py .

EXPOSE 5000

ENTRYPOINT python run.py
