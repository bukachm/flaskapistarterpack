from api.web.app import app
from api.web.config import config
from api.models import db

if __name__ == "__main__":
    db.create_all()
    app.run(host=config.HOST, port=config.PORT, threaded=config.THREADED)
