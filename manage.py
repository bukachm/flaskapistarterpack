from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from api.models import db
from api.web.app import app
from run import config

app.config['SQLALCHEMY_DATABASE_URI'] = config.SQLALCHEMY_DATABASE_URI

migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
